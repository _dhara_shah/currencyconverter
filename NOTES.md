Structure used:
- MVP with interactors and routers.

WHY: 
- Because it is easy to test interactors, and presenters and datasource in real projects, 
since everything is separated and not coupled tightly
- The idea is to keep the presenter as flat as possible
- Also, the interactor is the one that holds all the business logic

- Used dagger for dependency injections
- Used retrofit to make the network call.

Things that can be done better:
- The spinner, can be converted to a textview or another view, and on its tap, open a dialogfragment
- The dialogfragment can also be implemented in MVP form with interactors, and routers.
- USD base currency is fixed for now, can be made dynamic
