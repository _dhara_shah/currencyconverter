package com.dhara.currencyconverter;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.dhara.currencyconverter.dagger2.components.ActivityComponent;
import com.dhara.currencyconverter.dagger2.components.DaggerActivityComponent;
import com.dhara.currencyconverter.dagger2.modules.ActivityModule;

public abstract class BaseActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public ActivityComponent getActivityComponent() {
        return DaggerActivityComponent.builder()
                .currencyAppComponent(((CurrencyConverterApp)getApplication()).getCurrencyAppComponent())
                .appComponent(((CurrencyConverterApp) getApplication()).getAppComponent())
                .activityModule(new ActivityModule(this))
                .build();
    }
}