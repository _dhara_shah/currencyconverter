package com.dhara.currencyconverter;

import android.app.Application;
import android.content.Context;
import android.support.annotation.VisibleForTesting;

import com.dhara.currencyconverter.dagger2.components.AppComponent;
import com.dhara.currencyconverter.dagger2.components.DaggerAppComponent;
import com.dhara.currencyconverter.dagger2.components.DaggerCurrencyAppComponent;
import com.dhara.currencyconverter.dagger2.components.DaggerNetComponent;
import com.dhara.currencyconverter.dagger2.components.NetComponent;
import com.dhara.currencyconverter.dagger2.components.CurrencyAppComponent;
import com.dhara.currencyconverter.dagger2.injector.AppComponentProvider;
import com.dhara.currencyconverter.dagger2.modules.AppModule;
import com.dhara.currencyconverter.dagger2.modules.CurrencyAppModule;
import com.dhara.currencyconverter.dagger2.modules.NetModule;
import com.dhara.currencyconverter.network.api.Api;

public class CurrencyConverterApp extends Application implements AppComponentProvider {
    private static CurrencyConverterApp INSTANCE;
    private AppComponent appComponent;
    private NetComponent netComponent;
    private CurrencyAppComponent currencyAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        CurrencyConverterApp.init(this);
        getAppComponent().inject(this);
    }

    public static CurrencyConverterApp getInstance() {
        return INSTANCE;
    }

    @VisibleForTesting
    public static void init(final Context context) {
        if (INSTANCE == null) {
            INSTANCE = (CurrencyConverterApp) context;
        }
    }

    public AppComponent getAppComponent() {
        if (appComponent == null) {
            appComponent = DaggerAppComponent.builder()
                    .appModule(new AppModule(this))
                    .build();
        }
        return appComponent;
    }

    public CurrencyAppComponent getCurrencyAppComponent() {
        if (currencyAppComponent == null) {
            currencyAppComponent = DaggerCurrencyAppComponent.builder()
                    .appModule(new AppModule(this))
                    .currencyAppModule(new CurrencyAppModule())
                    .build();
        }
        return currencyAppComponent;
    }

    public NetComponent getNetComponent() {
        if (netComponent == null) {
            netComponent = DaggerNetComponent.builder()
                    .appModule(new AppModule(this))
                    .netModule(new NetModule(Api.getHost()))
                    .build();
        }
        return netComponent;
    }

    @Override
    public AppComponent getAppComponent(Context context) {
        return getAppComponent();
    }

    @Override
    public CurrencyAppComponent getCurrencyAppComponent(Context context) {
        return getCurrencyAppComponent();
    }

    @Override
    public NetComponent getNetComponent(Context context) {
        return getNetComponent();
    }
}
