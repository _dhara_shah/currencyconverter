package com.dhara.currencyconverter.dagger2.components;

import com.dhara.currencyconverter.dagger2.injection.PerActivity;
import com.dhara.currencyconverter.dagger2.modules.ActivityModule;
import com.dhara.currencyconverter.home.view.MainActivity;

import dagger.Component;

@PerActivity
@Component(modules = {ActivityModule.class}, dependencies = {AppComponent.class, CurrencyAppComponent.class})
public interface ActivityComponent {
    void inject(MainActivity mainActivity);
}
