package com.dhara.currencyconverter.dagger2.components;

import android.app.Application;

import com.dhara.currencyconverter.CurrencyConverterApp;
import com.dhara.currencyconverter.dagger2.modules.AppModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {
    void inject(CurrencyConverterApp currencyApp);

    Application application();
}
