package com.dhara.currencyconverter.dagger2.components;

import com.dhara.currencyconverter.dagger2.modules.AppModule;
import com.dhara.currencyconverter.dagger2.modules.CurrencyAppModule;
import com.dhara.currencyconverter.home.model.MainInteractorImpl;
import com.dhara.currencyconverter.home.presenter.MainPresenterImpl;

import dagger.Component;

@Component(modules = {CurrencyAppModule.class, AppModule.class})
public interface CurrencyAppComponent {
    void inject(MainPresenterImpl presenter);

    void inject(MainInteractorImpl mainInteractor);
}
