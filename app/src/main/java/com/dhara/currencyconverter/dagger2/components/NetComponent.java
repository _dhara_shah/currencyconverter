package com.dhara.currencyconverter.dagger2.components;

import com.dhara.currencyconverter.dagger2.modules.AppModule;
import com.dhara.currencyconverter.dagger2.modules.NetModule;
import com.dhara.currencyconverter.home.datasource.MainDataSourceImpl;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, NetModule.class})
public interface NetComponent {
    void inject(MainDataSourceImpl mainDataSource);
}
