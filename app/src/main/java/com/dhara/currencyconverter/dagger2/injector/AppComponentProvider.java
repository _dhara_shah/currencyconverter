package com.dhara.currencyconverter.dagger2.injector;

import android.content.Context;

import com.dhara.currencyconverter.dagger2.components.AppComponent;
import com.dhara.currencyconverter.dagger2.components.CurrencyAppComponent;
import com.dhara.currencyconverter.dagger2.components.NetComponent;

public interface AppComponentProvider {
    AppComponent getAppComponent(Context context);

    CurrencyAppComponent getCurrencyAppComponent(Context context);

    NetComponent getNetComponent(Context context);
}
