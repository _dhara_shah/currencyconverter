package com.dhara.currencyconverter.dagger2.injector;

import android.content.Context;

import com.dhara.currencyconverter.dagger2.components.CurrencyAppComponent;

public class CurrencyAppInjector {
    private CurrencyAppInjector() {

    }

    public static CurrencyAppComponent from(Context context) {
        final AppComponentProvider provider = (AppComponentProvider) context.getApplicationContext();
        return provider.getCurrencyAppComponent(context);
    }
}
