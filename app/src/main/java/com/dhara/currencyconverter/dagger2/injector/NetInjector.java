package com.dhara.currencyconverter.dagger2.injector;

import android.content.Context;

import com.dhara.currencyconverter.dagger2.components.NetComponent;

public class NetInjector {
    private NetInjector() {

    }

    public static NetComponent from(Context context) {
        final AppComponentProvider provider = (AppComponentProvider) context.getApplicationContext();
        return provider.getNetComponent(context);
    }
}
