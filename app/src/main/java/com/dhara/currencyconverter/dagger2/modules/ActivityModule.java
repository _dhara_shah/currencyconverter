package com.dhara.currencyconverter.dagger2.modules;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import com.dhara.currencyconverter.dagger2.injection.PerActivity;
import com.dhara.currencyconverter.dagger2.scope.ActivityScope;
import com.dhara.currencyconverter.home.presenter.MainPresenter;
import com.dhara.currencyconverter.home.presenter.MainPresenterImpl;
import com.dhara.currencyconverter.home.view.MainView;
import com.dhara.currencyconverter.home.view.MainViewImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class ActivityModule {
    private AppCompatActivity activity;

    public ActivityModule(AppCompatActivity activity) {
        this.activity = activity;
    }

    @Provides
    AppCompatActivity provideActivity() {
        return activity;
    }

    @Provides
    @ActivityScope
    Context providesContext() {
        return activity;
    }

    @Provides
    @PerActivity
    public MainPresenter provideMainPresenter(){
        return new MainPresenterImpl();
    }

    @Provides
    @PerActivity public MainView provideMainView() {
        return new MainViewImpl(activity);
    }
}