package com.dhara.currencyconverter.dagger2.modules;

import android.app.Application;

import com.dhara.currencyconverter.CurrencyConverterApp;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Singleton
@Module
public class AppModule {
    private final CurrencyConverterApp application;

    public AppModule(final CurrencyConverterApp application) {
        this.application = application;
    }

    @Provides
    Application provideApplication() {
        return application;
    }
}
