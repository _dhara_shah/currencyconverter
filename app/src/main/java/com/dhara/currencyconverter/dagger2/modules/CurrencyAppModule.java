package com.dhara.currencyconverter.dagger2.modules;

import com.dhara.currencyconverter.home.datasource.MainDataSource;
import com.dhara.currencyconverter.home.datasource.MainDataSourceImpl;
import com.dhara.currencyconverter.home.model.MainInteractor;
import com.dhara.currencyconverter.home.model.MainInteractorImpl;
import com.google.gson.Gson;

import dagger.Module;
import dagger.Provides;

@Module
public class CurrencyAppModule {
    @Provides
    public Gson provideGson() {
        return new Gson();
    }

    @Provides
    public MainInteractor provideMainInteractor() {
        return new MainInteractorImpl();
    }

    @Provides
    public MainDataSource provideMainDataSource() {
        return new MainDataSourceImpl();
    }
}
