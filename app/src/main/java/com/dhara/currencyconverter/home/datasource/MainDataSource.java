package com.dhara.currencyconverter.home.datasource;

import com.dhara.currencyconverter.network.Listener;

import java.util.HashMap;

public interface MainDataSource {
    void fetchCurrencies(Listener<HashMap<String, Double>> listener);
}
