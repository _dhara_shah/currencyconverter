package com.dhara.currencyconverter.home.datasource;

import com.dhara.currencyconverter.CurrencyConverterApp;
import com.dhara.currencyconverter.dagger2.injector.NetInjector;
import com.dhara.currencyconverter.network.Listener;
import com.dhara.currencyconverter.network.ServiceError;
import com.dhara.currencyconverter.network.api.RestApi;
import com.dhara.currencyconverter.network.entity.CurrencyResponse;

import java.util.HashMap;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainDataSourceImpl implements MainDataSource {
    @Inject
    RestApi restApiService;

    public MainDataSourceImpl() {
        NetInjector.from(CurrencyConverterApp.getInstance()).inject(this);
    }

    @Override
    public void fetchCurrencies(final Listener<HashMap<String, Double>> listener) {
        restApiService.getCurrencyRates("USD").enqueue(new Callback<CurrencyResponse>() {
            @Override
            public void onResponse(final Call<CurrencyResponse> call,
                                   final Response<CurrencyResponse> response) {
                listener.onSuccess(response.body().getRates());
            }

            @Override
            public void onFailure(final Call<CurrencyResponse> call, final Throwable t) {
                listener.onError(new ServiceError(t.getMessage()));
            }
        });
    }
}
