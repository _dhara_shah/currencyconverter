package com.dhara.currencyconverter.home.model;

import com.dhara.currencyconverter.network.ServiceError;
import com.dhara.currencyconverter.network.entity.CurrencyRate;

import java.util.List;

public interface MainInteractor {
    void fetchCurrencies(ResponseListener responseListener);

    double calculate(String amount);

    void setSelectedCurrency(int position);

    List<CurrencyRate> getCurrencyList();

    interface ResponseListener {
        void onSuccess();

        void onError(ServiceError error);
    }
}
