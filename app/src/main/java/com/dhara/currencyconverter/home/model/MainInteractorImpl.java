package com.dhara.currencyconverter.home.model;

import com.dhara.currencyconverter.CurrencyConverterApp;
import com.dhara.currencyconverter.dagger2.injector.CurrencyAppInjector;
import com.dhara.currencyconverter.home.datasource.MainDataSource;
import com.dhara.currencyconverter.network.Listener;
import com.dhara.currencyconverter.network.ServiceError;
import com.dhara.currencyconverter.network.entity.CurrencyRate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

public class MainInteractorImpl implements MainInteractor {
    @Inject
    MainDataSource mainDataSource;

    private final List<CurrencyRate> currencyList;
    private CurrencyRate selectedCurrency;

    public MainInteractorImpl() {
        CurrencyAppInjector.from(CurrencyConverterApp.getInstance()).inject(this);
        currencyList = new ArrayList<>();
    }

    @Override
    public void fetchCurrencies(final ResponseListener responseListener) {
        mainDataSource.fetchCurrencies(new Listener<HashMap<String, Double>>() {
            @Override
            public void onSuccess(final HashMap<String, Double> response) {
                convertMapToList(response);
                responseListener.onSuccess();
            }

            @Override
            public void onError(final ServiceError error) {
                responseListener.onError(error);
            }
        });
    }

    @Override
    public double calculate(final String amount) {
        if (selectedCurrency == null) {
            return 0;
        }
        return Double.parseDouble(amount) * selectedCurrency.getRate();
    }

    @Override
    public void setSelectedCurrency(final int position) {
        if (!getCurrencyList().isEmpty()) {
            selectedCurrency = getCurrencyList().get(position);
        }
    }

    @Override
    public List<CurrencyRate> getCurrencyList() {
        return currencyList == null || currencyList.isEmpty() ? Collections.emptyList() : currencyList;
    }

    private void convertMapToList(final Map<String, Double> response) {
        for (final Map.Entry<String, Double> entry : response.entrySet()) {
            final CurrencyRate rate = new CurrencyRate(entry.getKey(), entry.getValue());
            currencyList.add(rate);
        }
    }
}
