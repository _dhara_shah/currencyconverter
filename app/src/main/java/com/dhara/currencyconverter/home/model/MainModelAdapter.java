package com.dhara.currencyconverter.home.model;

import com.dhara.currencyconverter.network.entity.CurrencyRate;

import java.util.List;

public interface MainModelAdapter {
    int getCount();

    String getCurrency(int position);

    List<CurrencyRate> getCurrencyList();
}
