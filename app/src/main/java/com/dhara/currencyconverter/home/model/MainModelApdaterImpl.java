package com.dhara.currencyconverter.home.model;

import com.dhara.currencyconverter.network.entity.CurrencyRate;

import java.util.Collections;
import java.util.List;

public class MainModelApdaterImpl implements MainModelAdapter {
    private final List<CurrencyRate> currencyRateList;

    public MainModelApdaterImpl(final List<CurrencyRate> currencyRateList) {
        this.currencyRateList = currencyRateList;
    }

    @Override
    public int getCount() {
        return isEmpty() ? 0 : currencyRateList.size();
    }

    @Override
    public String getCurrency(final int position) {
        return isEmpty() ? "" : currencyRateList.get(position).toString();
    }

    @Override
    public List<CurrencyRate> getCurrencyList() {
        return isEmpty() ? Collections.emptyList() : currencyRateList;
    }

    private boolean isEmpty() {
        return currencyRateList == null || currencyRateList.isEmpty();
    }
}
