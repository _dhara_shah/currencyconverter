package com.dhara.currencyconverter.home.presenter;

import com.dhara.currencyconverter.home.view.MainView;

public interface MainPresenter {
    void handleOnCreate(final MainView mainView);
}
