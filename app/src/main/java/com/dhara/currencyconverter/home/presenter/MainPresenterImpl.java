package com.dhara.currencyconverter.home.presenter;

import com.dhara.currencyconverter.CurrencyConverterApp;
import com.dhara.currencyconverter.dagger2.injector.CurrencyAppInjector;
import com.dhara.currencyconverter.home.model.MainInteractor;
import com.dhara.currencyconverter.home.model.MainModelApdaterImpl;
import com.dhara.currencyconverter.home.view.MainView;
import com.dhara.currencyconverter.home.view.ViewInteractionListener;
import com.dhara.currencyconverter.network.ServiceError;

import javax.inject.Inject;

public class MainPresenterImpl implements MainPresenter, ViewInteractionListener, MainInteractor.ResponseListener {
    private MainView mainView;

    @Inject
    MainInteractor mainInteractor;

    public MainPresenterImpl() {
        CurrencyAppInjector.from(CurrencyConverterApp.getInstance()).inject(this);
    }

    @Override
    public void handleOnCreate(final MainView mainView) {
        this.mainView = mainView;

        mainView.initViews(new MainModelApdaterImpl(mainInteractor.getCurrencyList()), this);
        mainView.showProgress();
        mainInteractor.fetchCurrencies(this);
    }

    @Override
    public void onConvertClicked(final String amount) {
        mainView.setConvertedValue(mainInteractor.calculate(amount));
    }

    @Override
    public void onCurrencySelected(final int position) {
        mainInteractor.setSelectedCurrency(position);
        mainView.setCurrency(position);
    }

    @Override
    public void onSuccess() {
        mainView.hideProgress();
        mainView.updateData(new MainModelApdaterImpl(mainInteractor.getCurrencyList()));
    }

    @Override
    public void onError(final ServiceError error) {
        mainView.hideProgress();
        mainView.showError(error.getErrorMessage());
    }
}
