package com.dhara.currencyconverter.home.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.dhara.currencyconverter.R;
import com.dhara.currencyconverter.home.model.MainModelAdapter;
import com.dhara.currencyconverter.network.entity.CurrencyRate;

public class CurrencyAdapter extends ArrayAdapter<CurrencyRate> {
    private final MainModelAdapter mainModelAdapter;
    private LayoutInflater inflater;

    public CurrencyAdapter(final Context context,
                           final MainModelAdapter mainModelAdapter) {
        super(context, R.layout.individual_row, R.id.txt_currency, mainModelAdapter.getCurrencyList());
        this.inflater = LayoutInflater.from(context);
        this.mainModelAdapter = mainModelAdapter;
    }

    @Override
    public View getDropDownView(final int position, @Nullable final View convertView,
                                @NonNull final ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.individual_row, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.txtCurrency.setText(mainModelAdapter.getCurrency(position));
        return convertView;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtCurrency;

        ViewHolder(final View itemView) {
            super(itemView);
            txtCurrency = itemView.findViewById(R.id.txt_currency);
        }
    }
}
