package com.dhara.currencyconverter.home.view;

import android.os.Bundle;

import com.dhara.currencyconverter.BaseActivity;
import com.dhara.currencyconverter.R;
import com.dhara.currencyconverter.home.presenter.MainPresenter;

import javax.inject.Inject;

public class MainActivity extends BaseActivity {

    @Inject
    MainView mainView;

    @Inject
    MainPresenter mainPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
        setContentView(R.layout.activity_main);
        mainPresenter.handleOnCreate(mainView);
    }
}
