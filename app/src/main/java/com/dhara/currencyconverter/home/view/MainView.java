package com.dhara.currencyconverter.home.view;

import com.dhara.currencyconverter.home.model.MainModelAdapter;

public interface MainView {
    void initViews(final MainModelAdapter modelAdapter, final ViewInteractionListener listener);

    void showError(final String errorMessage);

    void setCurrency(int position);

    void updateData(final MainModelAdapter mainModelAdapter);

    void showProgress();

    void hideProgress();

    void setConvertedValue(double value);
}
