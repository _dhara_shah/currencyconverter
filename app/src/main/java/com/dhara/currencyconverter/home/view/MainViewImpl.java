package com.dhara.currencyconverter.home.view;

import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.dhara.currencyconverter.R;
import com.dhara.currencyconverter.home.model.MainModelAdapter;

import javax.inject.Inject;

public class MainViewImpl implements MainView {
    private final AppCompatActivity activity;
    private ViewInteractionListener listener;
    private Button btnConvert;
    private Spinner spinnerCurrency;
    private EditText etCurrency;
    private CurrencyAdapter adapter;
    private TextView txtCurrency;
    private View progressBar;

    @Inject
    public MainViewImpl(final AppCompatActivity activity) {
        this.activity = activity;
    }

    @Override
    public void initViews(final MainModelAdapter modelAdapter, final ViewInteractionListener listener) {
        this.listener = listener;
        btnConvert = activity.findViewById(R.id.btn_convert);
        txtCurrency = activity.findViewById(R.id.txt_converted);
        progressBar = activity.findViewById(R.id.progress_bar);
        spinnerCurrency = activity.findViewById(R.id.spinner_currency);
        etCurrency = activity.findViewById(R.id.et_currency);

        adapter = new CurrencyAdapter(activity, modelAdapter);
        spinnerCurrency.setAdapter(adapter);

        spinnerCurrency.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(final AdapterView<?> parent, final View view, final int position, final long id) {
                listener.onCurrencySelected(position);
            }

            @Override
            public void onNothingSelected(final AdapterView<?> parent) {

            }
        });

        btnConvert.setOnClickListener(v -> {
            if (isDataValid()) {
                listener.onConvertClicked(etCurrency.getText().toString());
            }
        });
    }

    @Override
    public void showError(final String errorMessage) {
        Snackbar.make(btnConvert, errorMessage, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void setCurrency(final int position) {
        spinnerCurrency.setSelection(position);
    }

    @Override
    public void updateData(final MainModelAdapter mainModelAdapter) {
        adapter = new CurrencyAdapter(activity, mainModelAdapter);
        spinnerCurrency.setAdapter(adapter);
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void setConvertedValue(final double value) {
        txtCurrency.setText(activity.getString(R.string.updated_value, value));
    }

    private boolean isDataValid() {
        return !TextUtils.isEmpty(etCurrency.getText().toString());
    }
}
