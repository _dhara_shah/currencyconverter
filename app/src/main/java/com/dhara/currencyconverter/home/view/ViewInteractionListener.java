package com.dhara.currencyconverter.home.view;

public interface ViewInteractionListener {
    void onConvertClicked(final String amount);

    void onCurrencySelected(final int position);
}
