package com.dhara.currencyconverter.network;

public interface Listener<T> {
    void onSuccess(T response);

    void onError(ServiceError error);
}
