package com.dhara.currencyconverter.network.api;

import android.support.annotation.VisibleForTesting;

public class Api {
    @VisibleForTesting
    public static String BASE_URL = "http://api.fixer.io/";

    private Api() { }

    public static String getHost() {
        return BASE_URL;
    }
}
