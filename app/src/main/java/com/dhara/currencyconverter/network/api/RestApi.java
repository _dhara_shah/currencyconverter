package com.dhara.currencyconverter.network.api;

import com.dhara.currencyconverter.network.entity.CurrencyResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RestApi {
    @GET("/latest")
    Call<CurrencyResponse> getCurrencyRates(@Query(value = "base", encoded = true) String baseCurrency);
}