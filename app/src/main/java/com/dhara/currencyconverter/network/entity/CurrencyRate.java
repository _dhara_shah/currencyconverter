package com.dhara.currencyconverter.network.entity;

public class CurrencyRate {
    private final String currency;
    private final double rate;

    public CurrencyRate(final String currency, final double rate) {
        this.currency = currency;
        this.rate = rate;
    }

    public double getRate() {
        return rate;
    }

    @Override
    public String toString() {
        return currency;
    }
}
