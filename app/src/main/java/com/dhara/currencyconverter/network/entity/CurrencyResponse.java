package com.dhara.currencyconverter.network.entity;

import java.util.HashMap;

public class CurrencyResponse {
    private HashMap<String, Double> rates;

    public HashMap<String, Double> getRates() {
        return rates;
    }
}
